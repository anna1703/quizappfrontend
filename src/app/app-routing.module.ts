import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import { AuthGuardService } from './helpers/auth-guard.service';

const routes: Routes = [{
  path: '',
  loadChildren: './pages/pages.module#PagesModule',
  canActivate: [ AuthGuardService ],
  canActivateChild: [ AuthGuardService ]
}, {
  path: 'auth',
  loadChildren: './auth/auth.module#AuthModule'
}, {
  path: '**',
  redirectTo: ''
}];

const config: ExtraOptions = {
  useHash: true
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

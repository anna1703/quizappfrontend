import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../core/service/user/auth.service';
import { equalFieldsValidator } from '../../core/validators/equal-fields.validator';
import { UserRegisterDto } from '../../core/model/user/user-register-dto';

@Component({
  selector: 'qa-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  form: FormGroup;

  constructor(private fb: FormBuilder,
              private authService: AuthService) {
    this.form = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: this.fb.group({
        password1: ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(25)])],
        password2: ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(25)])]
      }, {
        validator: equalFieldsValidator('password1', 'password2')
      }),
      firstname: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(35)])],
      lastname: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(35)])]
    });
  }

  getRegisterData(): UserRegisterDto {
    return this.form.value as UserRegisterDto;
  }

  register(): void {
    this.authService.register(this.getRegisterData());
  }
}

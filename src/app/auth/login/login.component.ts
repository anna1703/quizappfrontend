import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { CredentialsDto } from '../../core/model/user/credentials-dto';
import { AuthService } from '../../core/service/user/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'qa-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router: Router) {
    this.form = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required])],
    });
  }

  ngOnInit(): void {
    if (this.authService.isUserAuthenticated()) {
      this.router.navigate(['/dashboard']);
    }
  }

  getLoginData(): CredentialsDto {
    return this.form.value as CredentialsDto;
  }

  login(): void {
    this.authService.login(this.getLoginData());
  }
}

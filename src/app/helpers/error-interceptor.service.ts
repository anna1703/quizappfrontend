import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { AuthService } from '../core/service/user/auth.service';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MessageService } from '../core/service/common/message.service';
import { Router } from '@angular/router';

@Injectable()
export class ErrorInterceptorService implements HttpInterceptor {

  constructor(private authService: AuthService,
              private messageService: MessageService,
              private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(catchError(err => {
      if (err.status === 401) {
        this.authService.logout();
        this.messageService.displayErrorMessage('Token wygasł');
      }
      if (err.status === 404) {
        this.router.navigate(['/error/notFound'], { queryParams: { errorMsg: err.error.details || 'Nie znaleziono żądanego zasobu' }});
      }
      if (err.status === 500) {
        this.router.navigate(['/error/internalServer'], { queryParams: { errorMsg: err.error.details || 'Wystąpił błąd serwera' }});
      }
      const error = err.error.details || err.error.message || err.statusText || 'Wystąpił nieoczekiwany błąd';
      return throwError(error);
    }));
  }
}

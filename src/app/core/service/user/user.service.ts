import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserListDto } from '../../model/user/user-list-dto';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  readListByEmail(email: string): Observable<UserListDto> {
    let params: HttpParams = new HttpParams();
    params = params.append('email', email);
    return this.http.get<UserListDto>(`${environment.apiUrl}/user`, { params: params });
  }
}

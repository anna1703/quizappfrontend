import { Injectable } from '@angular/core';
import { Subscription} from 'rxjs';
import { CredentialsDto } from '../../model/user/credentials-dto';
import { HttpClient } from '@angular/common/http';
import { UserRegisterDto } from '../../model/user/user-register-dto';
import { SuccessResponseDto } from '../../model/common/success-response-dto';
import { environment } from '../../../../environments/environment';
import { TokenDto } from '../../model/user/token-dto';
import { MessageService } from '../common/message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  jwtHelper: JwtHelperService = new JwtHelperService();

  constructor(private http: HttpClient,
              private messageService: MessageService,
              private router: Router,
              private route: ActivatedRoute) { }

  static getToken(): string {
    return localStorage.getItem('token');
  }

  login(dto: CredentialsDto): Subscription {
    return this.http.post<TokenDto>(`${environment.apiUrl}/auth/login`, dto)
      .subscribe(
        success => {
          this.manageJwtToken(success.token);
          const url: string = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
          this.router.navigate([url]);
        },
        error => {
          this.messageService.displayErrorMessage(error);
        }
      );
  }

  logout(): void {
    this.clearUserData();
    this.router.navigate(['/auth/login']);
  }

  private clearUserData(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('email');
    localStorage.removeItem('firstname');
    localStorage.removeItem('lastname');
  }

  register(dto: UserRegisterDto): Subscription {
    return this.http.post<SuccessResponseDto>(`${environment.apiUrl}/auth/register`, dto)
      .subscribe(
        success => {
          this.messageService.displaySuccessMessage(success.message);
          this.router.navigate(['/auth/login']);
        },
        error => {
          this.messageService.displayErrorMessage(error);
        }
      );
  }

  isUserAuthenticated(): boolean {
    const token: string = AuthService.getToken();
    if (token == null || this.jwtHelper.isTokenExpired(token)) {
      this.clearUserData();
      return false;
    }
    return true;
  }

  private manageJwtToken(token: string): void {
    const parsedToken: string = this.jwtHelper.decodeToken(token);
    localStorage.setItem('token', token);
    localStorage.setItem('email', parsedToken['email']);
    localStorage.setItem('firstname', parsedToken['firstname']);
    localStorage.setItem('lastname', parsedToken['lastname']);
  }
}

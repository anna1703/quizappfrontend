import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StatisticsCommonDto } from '../../model/statistics/statistics-common-dto';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { StatisticsSolverDto } from '../../model/statistics/statistics-solver-dto';
import { StatisticsCreatorDto } from '../../model/statistics/statistics-creator-dto';
import { saveAs } from 'file-saver';
import { MessageService } from '../common/message.service';

@Injectable({
  providedIn: 'root'
})
export class StatisticsService {

  constructor(private http: HttpClient,
              private messageService: MessageService) { }

  readCommonStatistics(quizId: number): Observable<StatisticsCommonDto> {
    return this.http.get<StatisticsCommonDto>(`${environment.apiUrl}/statistics/common/${quizId}`);
  }

  readSolverStatistics(quizId: number): Observable<StatisticsSolverDto> {
    return this.http.get<StatisticsSolverDto>(`${environment.apiUrl}/statistics/solver/${quizId}`);
  }

  readCreatorStatistics(quizId: number): Observable<StatisticsCreatorDto> {
    return this.http.get<StatisticsCreatorDto>(`${environment.apiUrl}/statistics/creator/${quizId}`);
  }

  readFullStatistics(quizId: number): void {
    this.http.get(`${environment.apiUrl}/statistics/full/${quizId}`, { observe: 'response', responseType: 'blob' })
      .subscribe(
        res => {
          const contentType = res.body.type;
          const name = res.headers.get('content-disposition').split('=')[1];
          const blob = new Blob([res.body], { type: contentType });
          saveAs(blob, name);
        },
        error => {
          this.messageService.displayErrorMessage(error);
        }
      );
  }
}

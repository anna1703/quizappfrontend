import { Injectable } from '@angular/core';
import { saveAs } from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class AttachmentService {

  constructor() { }

  static saveAttachment(filename: string, readerResult: string): string {
    return filename + ';' + readerResult;
  }

  static downloadAttachment(attachment: string): void {
    if (attachment) {
      const byteCharacters: string = atob(attachment.split(',')[1]);
      const byteNumbers: ArrayBuffer = new ArrayBuffer(byteCharacters.length);
      const byteArray: Uint8Array = new Uint8Array(byteNumbers);

      for (let i = 0; i < byteCharacters.length; i++) {
        byteArray[i] = byteCharacters.charCodeAt(i);
      }

      const filename: string = attachment.split(';')[0];
      const contentType: string = (attachment.split(';')[1]).split(':')[1];
      const blob: Blob = new Blob([byteNumbers], { type: contentType });

      saveAs(blob, filename);
    }
  }

  static canDisplayInline(attachment: string): boolean {
    if (attachment == null) {
      return false;
    }
    return attachment.split(';')[1].split(':')[1].startsWith('image/');
  }

  static getInlineContent(attachment: string): string {
    if (attachment == null) {
      return '';
    }
    const parts = attachment.split(';');
    return parts[1] + ';' + parts[2];
  }
}

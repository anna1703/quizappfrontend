import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { QuestionDto } from '../../model/quiz/question-dto';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private http: HttpClient) { }

  create(dto: QuestionDto, quizId: number): Observable<QuestionDto> {
    return this.http.post<QuestionDto>(`${environment.apiUrl}/quiz/${quizId}/question`, dto);
  }

  read(id: number): Observable<QuestionDto> {
    return this.http.get<QuestionDto>(`${environment.apiUrl}/question/${id}`);
  }

  update(id: number, dto: QuestionDto): Observable<QuestionDto> {
    return this.http.put<QuestionDto>(`${environment.apiUrl}/question/${id}`, dto);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${environment.apiUrl}/question/${id}`);
  }
}

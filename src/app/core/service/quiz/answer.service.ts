import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AnswerDto } from '../../model/quiz/answer-dto';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  constructor(private http: HttpClient) { }

  create(dto: AnswerDto, questionId: number): Observable<AnswerDto> {
    return this.http.post<AnswerDto>(`${environment.apiUrl}/question/${questionId}/answer`, dto);
  }

  read(id: number): Observable<AnswerDto> {
    return this.http.get<AnswerDto>(`${environment.apiUrl}/answer/${id}`);
  }

  update(id: number, dto: AnswerDto): Observable<AnswerDto> {
    return this.http.put<AnswerDto>(`${environment.apiUrl}/answer/${id}`, dto);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${environment.apiUrl}/answer/${id}`);
  }
}

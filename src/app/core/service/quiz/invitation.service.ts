import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { InvitationDto } from '../../model/quiz/invitation-dto';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { InvitationListDto } from '../../model/quiz/invitation-list-dto';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class InvitationService {

  constructor(private http: HttpClient) { }

  create(dto: InvitationDto, quizId: number, userId: number): Observable<InvitationDto> {
    return this.http.post<InvitationDto>(`${environment.apiUrl}/quiz/${quizId}/user/${userId}/invitation`, dto);
  }

  readForQuizList(quizId: number, page: number, size: number): Observable<InvitationListDto> {
    let params: HttpParams = new HttpParams();
    params = params.append('page', page.toString());
    params = params.append('size', size.toString());
    return this.http.get<InvitationListDto>(`${environment.apiUrl}/quiz/${quizId}/invitation`, { params: params });
  }

  readForUser(page: number, size: number): Observable<InvitationListDto> {
    let params: HttpParams = new HttpParams();
    params = params.append('page', page.toString());
    params = params.append('size', size.toString());
    return this.http.get<InvitationListDto>(`${environment.apiUrl}/invitation`, { params: params });
  }

  isInvitationValid(invitation: InvitationDto): boolean {
    const dateFromOk = invitation.dateFrom == null || moment().isAfter(moment(invitation.dateFrom));
    const dateToOk = invitation.dateTo == null || moment().isBefore(moment(invitation.dateTo));
    return dateFromOk && dateToOk;
  }
}

import { Injectable } from '@angular/core';
import { QuizDto } from '../../model/quiz/quiz-dto';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { QuizFullDto } from '../../model/quiz/quiz-full-dto';
import { QuizListDto } from '../../model/quiz/quiz-list-dto';
import { QuizParams } from '../../model/quiz/quiz-params';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  constructor(private http: HttpClient) { }

  create(dto: QuizDto): Observable<QuizDto> {
    return this.http.post<QuizDto>(`${environment.apiUrl}/quiz`, dto);
  }

  readWithDetails(id: number): Observable<QuizFullDto> {
    return this.http.get<QuizFullDto>(`${environment.apiUrl}/quiz/${id}/details`);
  }

  update(id: number, dto: QuizDto): Observable<QuizDto> {
    return this.http.put<QuizDto>(`${environment.apiUrl}/quiz/${id}`, dto);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${environment.apiUrl}/quiz/${id}`);
  }

  publish(id: number): Observable<QuizDto> {
    return this.http.put<QuizDto>(`${environment.apiUrl}/quiz/${id}/publish`, {});
  }

  readCreatedList(page: number, size: number, quizParams: QuizParams): Observable<QuizListDto> {
    let params: HttpParams = new HttpParams();
    params = params.append('page', page.toString());
    params = params.append('size', size.toString());
    params = quizParams.name ? params.append('name', quizParams.name) : params;
    params = quizParams.description ? params.append('description', quizParams.description) : params;
    params = quizParams.inPreparation != null ? params.append('inPreparation', quizParams.inPreparation.toString()) : params;
    return this.http.get<QuizListDto>(`${environment.apiUrl}/quiz/created`, { params: params });
  }

  readPublicList(page: number, size: number, quizParams: QuizParams): Observable<QuizListDto> {
    let params: HttpParams = new HttpParams();
    params = params.append('page', page.toString());
    params = params.append('size', size.toString());
    params = quizParams.name ? params.append('name', quizParams.name) : params;
    params = quizParams.description ? params.append('description', quizParams.description) : params;
    params = quizParams.creatorFirstname ? params.append('creatorFirstname', quizParams.creatorFirstname) : params;
    params = quizParams.creatorLastname ? params.append('creatorLastname', quizParams.creatorLastname) : params;
    return this.http.get<QuizListDto>(`${environment.apiUrl}/quiz/public`, { params: params });
  }

  readSolvedList(page: number, size: number, quizParams: QuizParams): Observable<QuizListDto> {
    let params: HttpParams = new HttpParams();
    params = params.append('page', page.toString());
    params = params.append('size', size.toString());
    params = quizParams.name ? params.append('name', quizParams.name) : params;
    params = quizParams.description ? params.append('description', quizParams.description) : params;
    params = quizParams.creatorFirstname ? params.append('creatorFirstname', quizParams.creatorFirstname) : params;
    params = quizParams.creatorLastname ? params.append('creatorLastname', quizParams.creatorLastname) : params;
    return this.http.get<QuizListDto>(`${environment.apiUrl}/quiz/solved`, { params: params });
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { MessageService } from '../common/message.service';
import { Router } from '@angular/router';
import { TrialQuestionDto } from '../../model/trial/trial-question-dto';
import { Observable } from 'rxjs';
import { TrialAnswerDto } from '../../model/trial/trial-answer-dto';

@Injectable({
  providedIn: 'root'
})
export class TrialService {

  constructor(private http: HttpClient,
              private router: Router,
              private messageService: MessageService) { }

  solveQuiz(quizId: number): void {
    this.http.get<Object>(`${environment.apiUrl}/quiz/${quizId}/solve`)
      .subscribe(
        trialId => {
          this.router.navigate(['/trial', trialId]);
        },
        error => {
          this.messageService.displayErrorMessage(error);
        }
      );
  }

  getCurrentQuestion(trialId: number): Observable<TrialQuestionDto> {
    return this.http.get<TrialQuestionDto>(`${environment.apiUrl}/trial/${trialId}/current`);
  }

  answerCurrentQuestion(trialId: number, trialAnswerDto: TrialAnswerDto): Observable<boolean> {
    return this.http.post<boolean>(`${environment.apiUrl}/trial/${trialId}/current`, trialAnswerDto);
  }
}

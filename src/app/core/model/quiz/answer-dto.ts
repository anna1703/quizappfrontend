import { Dto } from '../common/dto';

export interface AnswerDto extends Dto {
  content: string;
  isCorrect: boolean;
  attachment?: string;
}

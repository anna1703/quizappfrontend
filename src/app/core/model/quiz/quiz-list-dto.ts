import { QuizDto } from './quiz-dto';

export interface QuizListDto {
  quizzes: Array<QuizDto>;
  totalCount: number;
}

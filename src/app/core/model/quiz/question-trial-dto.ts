import { Dto } from '../common/dto';

export interface QuestionTrialDto extends Dto {
  content: string;
  points: number;
  attachment?: string;
}

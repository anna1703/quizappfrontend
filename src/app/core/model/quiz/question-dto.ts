import { Dto } from '../common/dto';

export interface QuestionDto extends Dto {
  content: string;
  points: number;
  answersToDraw: number;
  attachment?: string;
}

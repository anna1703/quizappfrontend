import { InvitationDto } from './invitation-dto';

export interface InvitationListDto {
  invitations: Array<InvitationDto>;
  totalCount: number;
}

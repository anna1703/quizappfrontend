import { Dto } from '../common/dto';
import { QuizDto } from './quiz-dto';
import { UserDto } from '../user/user-dto';
import * as moment from 'moment';

export interface InvitationDto extends Dto {
  dateFrom: moment.Moment;
  dateTo: moment.Moment;
  quiz: QuizDto;
  user: UserDto;
}

export interface QuizParams {
  name: string;
  description: string;
  inPreparation: boolean;
  creatorFirstname: string;
  creatorLastname: string;
}

import { QuizDto } from './quiz-dto';
import { QuestionFullDto } from './question-full-dto';

export interface QuizFullDto {
  quiz: QuizDto;
  questions: Array<QuestionFullDto>;
}

import { Dto } from '../common/dto';

export interface AnswerTrialDto extends Dto {
  content: string;
  attachment?: string;
}

import { QuestionDto } from './question-dto';
import { AnswerDto } from './answer-dto';

export interface QuestionFullDto {
  question: QuestionDto;
  answers: Array<AnswerDto>;
}

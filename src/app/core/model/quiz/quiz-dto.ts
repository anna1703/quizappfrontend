import { Dto } from '../common/dto';
import { UserDto } from '../user/user-dto';

export interface QuizDto extends Dto {
  name: string;
  description: string;
  isPrivate: boolean;
  possibleTrials: number;
  time: number;
  questionsToDraw: number;
  inPreparation: boolean;
  creator: UserDto;
}

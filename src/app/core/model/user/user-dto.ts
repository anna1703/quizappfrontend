import { Dto } from '../common/dto';

export interface UserDto extends Dto {
  email: string;
  firstname: string;
  lastname: string;
}

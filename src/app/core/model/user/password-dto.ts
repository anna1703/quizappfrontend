export interface PasswordDto {
  password1: string;
  password2: string;
}

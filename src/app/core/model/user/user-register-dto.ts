import { PasswordDto } from './password-dto';

export interface UserRegisterDto {
  email: string;
  password: PasswordDto;
  firstname: string;
  lastname: string;
}

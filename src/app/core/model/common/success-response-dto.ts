import { Dto } from './dto';

export interface SuccessResponseDto extends Dto {
  message: string;
}

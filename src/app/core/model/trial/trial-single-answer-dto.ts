import { Dto } from '../common/dto';

export interface TrialSingleAnswerDto extends Dto {
  isChecked: boolean;
}

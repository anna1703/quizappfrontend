import { Dto } from '../common/dto';
import { QuestionTrialDto } from '../quiz/question-trial-dto';
import { AnswerTrialDto } from '../quiz/answer-trial-dto';
import * as moment from 'moment';

export interface TrialQuestionDto extends Dto {
  quizId: number;
  time: number;
  startTime: moment.Moment;
  currentQuestionNumber: number;
  allQuestions: number;
  question: QuestionTrialDto;
  answers: Array<AnswerTrialDto>;
}

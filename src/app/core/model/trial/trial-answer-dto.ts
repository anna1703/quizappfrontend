import { TrialSingleAnswerDto } from './trial-single-answer-dto';

export interface TrialAnswerDto {
  answers: Array<TrialSingleAnswerDto>;
}

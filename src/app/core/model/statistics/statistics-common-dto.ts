export interface StatisticsCommonDto {
  quizId: number;
  quizName: string;
  quizDescription: string;

  trialsNumber: number;
  usersNumber: number;
  avgTime: string;

  /* uwzględnia wszystkie wyniki */
  minPoints: string;
  avgPoints: string;
  maxPoints: string;

  /* uzwględnia wyniki wysłane przed końcem czasu */
  minPointsTimeOk: string;
  avgPointsTimeOk: string;
  maxPointsTimeOk: string;
}

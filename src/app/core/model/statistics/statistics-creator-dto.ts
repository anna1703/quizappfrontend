import { StatisticsCommonDto } from './statistics-common-dto';
import { TrialStatisticsDto } from './trial-statistics-dto';

export interface StatisticsCreatorDto {
  commonStatistics: StatisticsCommonDto;
  topTrialStats: Array<TrialStatisticsDto>;
  topTrialStatsTimeOk: Array<TrialStatisticsDto>;
}

export interface TrialStatisticsDto {
  userName: string;
  isFinished: boolean;
  time: string;
  isTimeOk: boolean;
  points: string;
  pointsTimeOk: string;
  rank: number;
  rankTimeOk: number;
}

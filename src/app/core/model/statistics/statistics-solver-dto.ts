import { StatisticsCommonDto } from './statistics-common-dto';
import { TrialStatisticsDto } from './trial-statistics-dto';

export interface StatisticsSolverDto {
  commonStatistics: StatisticsCommonDto;
  trialsStats: Array<TrialStatisticsDto>;
}

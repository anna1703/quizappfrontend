import { AbstractControl, FormGroup } from '@angular/forms';
import * as moment from 'moment';

export function datesValidator(dateFromControlName: string, dateToControlName: string) {
  return (formGroup: FormGroup) => {
    const dateFromControl: AbstractControl = formGroup.get(dateFromControlName);
    const dateToControl: AbstractControl = formGroup.get(dateToControlName);

    if (dateFromControl.value == null || dateToControl.value == null) {
      // return if one field (or both) is empty
      return;
    }

    if (moment(dateFromControl.value).isAfter(moment(dateToControl.value))) {
      dateToControl.setErrors( { isAfter: true });
    } else {
      dateToControl.setErrors(null);
    }
  };
}

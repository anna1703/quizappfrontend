import { AbstractControl, ValidatorFn } from '@angular/forms';

export function propertyRequiredValidator(requiredProperty: string): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    return control.value[requiredProperty] ? null : {'propertyRequired': {value: control.value}};
  };
}

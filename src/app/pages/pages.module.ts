import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { AngularMaterialModule } from './angular-material/angular-material.module';

@NgModule({
  imports: [
    CommonModule,
    PagesRoutingModule,
    AngularMaterialModule
  ],
  declarations: [
    PagesComponent
  ]
})
export class PagesModule { }

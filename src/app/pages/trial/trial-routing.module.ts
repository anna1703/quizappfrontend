import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewTrialCurrentQuestionComponent } from './view-trial-current-question/view-trial-current-question.component';

const routes: Routes = [{
  path: ':id',
  component: ViewTrialCurrentQuestionComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrialRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { TrialService } from '../../../core/service/trial/trial.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TrialQuestionDto } from '../../../core/model/trial/trial-question-dto';
import { QuestionTrialDto } from '../../../core/model/quiz/question-trial-dto';
import { MessageService } from '../../../core/service/common/message.service';
import { AnswerTrialDto } from '../../../core/model/quiz/answer-trial-dto';
import { TrialAnswerDto } from '../../../core/model/trial/trial-answer-dto';
import { TrialSingleAnswerDto } from '../../../core/model/trial/trial-single-answer-dto';
import { timer } from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'qa-view-trial-current-question',
  templateUrl: './view-trial-current-question.component.html',
  styleUrls: ['./view-trial-current-question.component.scss']
})
export class ViewTrialCurrentQuestionComponent implements OnInit {

  trialId: number;
  trialQuestionDto: TrialQuestionDto = {
    id: null,
    quizId: null,
    time: null,
    startTime: null,
    currentQuestionNumber: 0,
    allQuestions: 0,
    question: { } as QuestionTrialDto,
    answers: []
  };
  trialAnswerDto: TrialAnswerDto = {
    answers: []
  };
  quizTime = '';
  afterTime = false;

  constructor(private trialService: TrialService,
              private router: Router,
              private route: ActivatedRoute,
              private messageService: MessageService) {
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
    };
  }

  ngOnInit() {
    this.trialId = +this.route.snapshot.paramMap.get('id');
    this.readQuestionTrialData(true);
  }

  readQuestionTrialData(isFirst: boolean): void {
    this.trialService.getCurrentQuestion(+this.route.snapshot.paramMap.get('id'))
      .subscribe(
        success => {
          this.trialQuestionDto = success;
          if (isFirst) {
            this.initTimer();
          }
          this.createTrialAnswerDto();
        },
        error => {
          this.messageService.displayErrorMessage(error);
          this.router.navigate(['/quiz/solved']);
        }
      );
  }

  initTimer(): void {
    timer(0, 1000)
      .subscribe(() => {
        const seconds = moment.duration(this.trialQuestionDto.time, 'minutes')
          .subtract(moment.duration(moment().diff(this.trialQuestionDto.startTime))).asSeconds();
        const transformedSeconds = Math.abs(Math.ceil(seconds));
        const hrs = Math.floor(Math.abs(transformedSeconds / 3600));
        const min = Math.floor(Math.abs((transformedSeconds - (3600 * hrs)) / 60));
        const sec = Math.abs(transformedSeconds - (3600 * hrs) - (60 * min));
        this.afterTime = seconds < 0;
        this.quizTime = hrs + ':' + (min < 10 ? '0' : '') + min + ':' + (sec < 10 ? '0' : '') + sec;
      });
  }

  createTrialAnswerDto(): void {
    this.trialAnswerDto = {
      answers: []
    };
    this.trialQuestionDto.answers.forEach(ta => {
      this.trialAnswerDto.answers.push({
        id: ta.id,
        isChecked: false
      } as TrialSingleAnswerDto);
    });
  }

  changeChecked(answer: AnswerTrialDto, $event) {
    this.trialAnswerDto.answers.find(e => e.id === answer.id).isChecked = $event;
  }

  sendAnswer(): void {
    this.trialService.answerCurrentQuestion(this.trialId, this.trialAnswerDto)
      .subscribe(
        hasNext => {
          if (hasNext) {
            this.readQuestionTrialData(false);
          } else {
            this.router.navigate(['/statistics/solver', this.trialQuestionDto.quizId]);
          }
          this.messageService.displaySuccessMessage('Zapisano odpowiedź');
        },
        error => {
          this.messageService.displayErrorMessage(error);
        }
      );
  }
}

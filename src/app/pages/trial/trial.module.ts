import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrialRoutingModule } from './trial-routing.module';
import { ViewTrialCurrentQuestionComponent } from './view-trial-current-question/view-trial-current-question.component';
import { ViewTrialQuestionComponent } from './view-trial-question/view-trial-question.component';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewTrialAnswerComponent } from './view-trial-answer/view-trial-answer.component';

@NgModule({
  declarations: [ViewTrialCurrentQuestionComponent, ViewTrialQuestionComponent, ViewTrialAnswerComponent],
  imports: [
    CommonModule,
    TrialRoutingModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class TrialModule { }

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AnswerTrialDto } from '../../../core/model/quiz/answer-trial-dto';
import { AttachmentService } from '../../../core/service/common/attachment.service';

@Component({
  selector: 'qa-view-trial-answer',
  templateUrl: './view-trial-answer.component.html',
  styleUrls: ['./view-trial-answer.component.scss']
})
export class ViewTrialAnswerComponent {

  @Input() order: number;
  @Input() trialAnswer: AnswerTrialDto;
  @Output() isChecked = new EventEmitter<boolean>();

  checked = false;

  constructor() { }

  change(): void {
    this.checked = !this.checked;
    this.isChecked.emit(this.checked);
  }

  shouldDisplayDownloadButton(): boolean {
    return this.trialAnswer.attachment != null && !this.shouldDisplayAttachmentInline();
  }

  shouldDisplayAttachmentInline(): boolean {
    return AttachmentService.canDisplayInline(this.trialAnswer.attachment);
  }

  downloadAttachment(): void {
    this.change(); // preventDefault tu nie zadziala niestety
    AttachmentService.downloadAttachment(this.trialAnswer.attachment);
  }

  displayInlineContent(): string {
    return AttachmentService.getInlineContent(this.trialAnswer.attachment);
  }

}

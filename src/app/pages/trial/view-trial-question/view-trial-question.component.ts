import { Component, Input } from '@angular/core';
import { TrialQuestionDto } from '../../../core/model/trial/trial-question-dto';
import { AttachmentService } from '../../../core/service/common/attachment.service';

@Component({
  selector: 'qa-view-trial-question',
  templateUrl: './view-trial-question.component.html',
  styleUrls: ['./view-trial-question.component.scss']
})
export class ViewTrialQuestionComponent {

  @Input() trialQuestion: TrialQuestionDto;

  constructor() { }

  shouldDisplayDownloadButton(): boolean {
    return this.trialQuestion.question.attachment != null && !this.shouldDisplayAttachmentInline();
  }

  shouldDisplayAttachmentInline(): boolean {
    return AttachmentService.canDisplayInline(this.trialQuestion.question.attachment);
  }

  downloadAttachment(): void {
    AttachmentService.downloadAttachment(this.trialQuestion.question.attachment);
  }

  displayInlineContent(): string {
    return AttachmentService.getInlineContent(this.trialQuestion.question.attachment);
  }
}

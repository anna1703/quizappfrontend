import { Component } from '@angular/core';
import { AuthService } from '../core/service/user/auth.service';

@Component({
  selector: 'qa-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent {

  constructor(private authService: AuthService) { }

  logout(): void {
    this.authService.logout();
  }
}

import { Component, OnInit } from '@angular/core';
import { StatisticsSolverDto } from '../../../core/model/statistics/statistics-solver-dto';
import { StatisticsService } from '../../../core/service/statistics/statistics.service';
import { MessageService } from '../../../core/service/common/message.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'qa-view-solver-statistics',
  templateUrl: './view-solver-statistics.component.html',
  styleUrls: ['./view-solver-statistics.component.scss']
})
export class ViewSolverStatisticsComponent implements OnInit {

  statistics: StatisticsSolverDto = {
    commonStatistics: { },
    trialsStats: []
  } as StatisticsSolverDto;

  constructor(private statisticsService: StatisticsService,
              private messageService: MessageService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.statisticsService.readSolverStatistics(+this.route.snapshot.paramMap.get('id'))
      .subscribe(
        statistics => {
          this.statistics = statistics;
        },
        error => {
          this.messageService.displayErrorMessage(error);
          this.router.navigate(['/quiz/solved']);
        }
      );
  }

  goBackToList(): void {
    this.router.navigate(['/quiz/solved']);
  }
}

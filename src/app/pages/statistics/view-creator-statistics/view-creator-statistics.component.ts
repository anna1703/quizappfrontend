import { Component, OnInit } from '@angular/core';
import { StatisticsService } from '../../../core/service/statistics/statistics.service';
import { MessageService } from '../../../core/service/common/message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { StatisticsCreatorDto } from '../../../core/model/statistics/statistics-creator-dto';

@Component({
  selector: 'qa-view-creator-statistics',
  templateUrl: './view-creator-statistics.component.html',
  styleUrls: ['./view-creator-statistics.component.scss']
})
export class ViewCreatorStatisticsComponent implements OnInit {

  statistics: StatisticsCreatorDto = {
    commonStatistics: { },
    topTrialStats: [],
    topTrialStatsTimeOk: []
  } as StatisticsCreatorDto;

  constructor(private statisticsService: StatisticsService,
              private messageService: MessageService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.statisticsService.readCreatorStatistics(+this.route.snapshot.paramMap.get('id'))
      .subscribe(
        statistics => {
          this.statistics = statistics;
        },
        error => {
          this.messageService.displayErrorMessage(error);
          this.router.navigate(['/quiz/created']);
        }
      );
  }

  goBackToList(): void {
    this.router.navigate(['/quiz/created']);
  }

  generateReport(): void {
    this.statisticsService.readFullStatistics(+this.route.snapshot.paramMap.get('id'));
  }
}

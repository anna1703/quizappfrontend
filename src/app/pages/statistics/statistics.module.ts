import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StatisticsRoutingModule } from './statistics-routing.module';
import { ViewCreatorStatisticsComponent } from './view-creator-statistics/view-creator-statistics.component';
import { ViewSolverStatisticsComponent } from './view-solver-statistics/view-solver-statistics.component';
import { ViewCommonStatisticsComponent } from './view-common-statistics/view-common-statistics.component';
import { AngularMaterialModule } from '../angular-material/angular-material.module';

@NgModule({
  declarations: [ViewCreatorStatisticsComponent, ViewSolverStatisticsComponent, ViewCommonStatisticsComponent],
  imports: [
    CommonModule,
    StatisticsRoutingModule,
    AngularMaterialModule
  ]
})
export class StatisticsModule { }

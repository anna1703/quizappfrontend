import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewCommonStatisticsComponent } from './view-common-statistics/view-common-statistics.component';
import { ViewCreatorStatisticsComponent } from './view-creator-statistics/view-creator-statistics.component';
import { ViewSolverStatisticsComponent } from './view-solver-statistics/view-solver-statistics.component';

const routes: Routes = [{
  path: 'common/:id',
  component: ViewCommonStatisticsComponent
}, {
  path: 'creator/:id',
  component: ViewCreatorStatisticsComponent
}, {
  path: 'solver/:id',
  component: ViewSolverStatisticsComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StatisticsRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { StatisticsService } from '../../../core/service/statistics/statistics.service';
import { ActivatedRoute, Router } from '@angular/router';
import { StatisticsCommonDto } from '../../../core/model/statistics/statistics-common-dto';
import { MessageService } from '../../../core/service/common/message.service';

@Component({
  selector: 'qa-view-common-statistics',
  templateUrl: './view-common-statistics.component.html',
  styleUrls: ['./view-common-statistics.component.scss']
})
export class ViewCommonStatisticsComponent implements OnInit {

  statistics: StatisticsCommonDto = { } as StatisticsCommonDto;

  constructor(private statisticsService: StatisticsService,
              private messageService: MessageService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.statisticsService.readCommonStatistics(+this.route.snapshot.paramMap.get('id'))
      .subscribe(
        statistics => {
          this.statistics = statistics;
        },
        error => {
          this.messageService.displayErrorMessage(error);
          this.router.navigate(['/quiz/public']);
        }
      );
  }

  goBackToList(): void {
    this.router.navigate(['/quiz/public']);
  }
}

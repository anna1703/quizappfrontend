import { Component, OnInit, ViewChild } from '@angular/core';
import { QuizService } from '../../../core/service/quiz/quiz.service';
import { QuizListDto } from '../../../core/model/quiz/quiz-list-dto';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material';
import { QuizParams } from '../../../core/model/quiz/quiz-params';
import { QuizView } from '../../../core/others/quiz-view.enum';

@Component({
  selector: 'qa-view-created-quizzes-list',
  templateUrl: './view-created-quizzes-list.component.html',
  styleUrls: ['./view-created-quizzes-list.component.scss']
})
export class ViewCreatedQuizzesListComponent implements OnInit {

  quizView = QuizView;
  form: FormGroup;
  filterData: QuizParams = { } as QuizParams;

  quizListDto: QuizListDto = {
    quizzes: [],
    totalCount: 0
  };

  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageSize = 10;
  pageIndex = 0;

  constructor(private fb: FormBuilder,
              private quizService: QuizService) {
    this.form = this.fb.group({
      name: [],
      description: [],
      inPreparation: []
    });
  }

  ngOnInit() {
    this.readList();
  }

  readList(): void {
    this.quizService.readCreatedList(this.pageIndex, this.pageSize, this.filterData)
      .subscribe(res => this.quizListDto = res);
  }

  changePage(e: any): void {
    this.pageSize = e.pageSize;
    this.pageIndex = e.pageIndex;
    this.readList();
  }

  filter(): void {
    this.filterData = this.form.value as QuizParams;
    this.pageIndex = 0;
    this.readList();
  }
}


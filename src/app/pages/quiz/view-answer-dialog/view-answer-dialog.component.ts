import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AnswerDto } from '../../../core/model/quiz/answer-dto';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AnswerService } from '../../../core/service/quiz/answer.service';
import { AttachmentService } from '../../../core/service/common/attachment.service';

@Component({
  selector: 'qa-view-answer-dialog',
  templateUrl: './view-answer-dialog.component.html',
  styleUrls: ['./view-answer-dialog.component.scss']
})
export class ViewAnswerDialogComponent implements OnInit {

  form: FormGroup;
  answerId: number;
  editable: boolean;
  answerDto: AnswerDto;
  attachment: string;

  constructor(private dialogRef: MatDialogRef<ViewAnswerDialogComponent>,
              private fb: FormBuilder,
              private answerService: AnswerService,
              @Inject(MAT_DIALOG_DATA) private data: any) {
    this.form = this.fb.group({
      content: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(1000)]],
      isCorrect: [false]
    });
    this.answerId = this.data['answerId'];
    this.editable = this.data['editable'];
    if (!this.editable) {
      this.form.disable();
    }
  }

  ngOnInit() {
    this.answerService.read(this.answerId)
      .subscribe(
        success => {
          this.answerDto = success;
          this.patchAnswerFormValue(success);
        },
        () => {
          this.dialogRef.close();
        }
      );
  }

  patchAnswerFormValue(dto: AnswerDto): void {
    this.form.patchValue({
      content: dto.content,
      isCorrect: dto.isCorrect
    });
    this.attachment = dto.attachment;
  }

  getAnswerData(): AnswerDto {
    const result: AnswerDto = this.form.value as AnswerDto;
    result.attachment = this.attachment;
    return result;
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  removeAttachment(): void {
    this.attachment = null;
  }

  downloadAttachment(): void {
    AttachmentService.downloadAttachment(this.attachment);
  }

  onFileChanged(event: Event) {
    const reader = new FileReader();
    if (event.target['files'] && event.target['files'].length === 1) {
      const file = event.target['files'][0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.attachment = AttachmentService.saveAttachment(file.name, reader.result as string);
      };
    }
  }
}

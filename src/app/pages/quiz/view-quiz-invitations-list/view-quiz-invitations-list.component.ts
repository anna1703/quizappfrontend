import { Component, OnInit, ViewChild } from '@angular/core';
import { InvitationService } from '../../../core/service/quiz/invitation.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatPaginator } from '@angular/material';
import { CreateInvitationDialogComponent } from '../create-invitation-dialog/create-invitation-dialog.component';
import { InvitationDto } from '../../../core/model/quiz/invitation-dto';
import { MessageService } from '../../../core/service/common/message.service';
import { InvitationListDto } from '../../../core/model/quiz/invitation-list-dto';

@Component({
  selector: 'qa-view-quiz-invitations-list',
  templateUrl: './view-quiz-invitations-list.component.html',
  styleUrls: ['./view-quiz-invitations-list.component.scss']
})
export class ViewQuizInvitationsListComponent implements OnInit {

  quizId: number;

  invitationListDto: InvitationListDto = {
    invitations: [],
    totalCount: 0
  };

  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageSize = 10;
  pageIndex = 0;

  constructor(private invitationService: InvitationService,
              private messageService: MessageService,
              private route: ActivatedRoute,
              private dialog: MatDialog) { }

  ngOnInit() {
    this.quizId = +this.route.snapshot.paramMap.get('id');
    this.readList();
  }

  openNewInvitationDialog(): void {
    const dialogRef = this.dialog.open(CreateInvitationDialogComponent, {
      width: '500px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.createInvitation(result);
      }
    });
  }

  createInvitation(invitationDto: InvitationDto): void {
    this.invitationService.create(invitationDto, this.quizId, invitationDto.user.id)
      .subscribe(
        () => {
          this.messageService.displaySuccessMessage('Zaproszenie zostało dodane');
          this.readList();
        },
        error => {
          this.messageService.displayErrorMessage(error);
        }
      );
  }

  readList(): void {
    this.invitationService.readForQuizList(this.quizId, this.pageIndex, this.pageSize)
      .subscribe(res => this.invitationListDto = res);
  }

  changePage(e: any): void {
    this.pageSize = e.pageSize;
    this.pageIndex = e.pageIndex;
    this.readList();
  }
}

import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { QuestionService } from '../../../core/service/quiz/question.service';
import { QuestionDto } from '../../../core/model/quiz/question-dto';
import { AttachmentService } from '../../../core/service/common/attachment.service';

@Component({
  selector: 'qa-view-question-dialog',
  templateUrl: './view-question-dialog.component.html',
  styleUrls: ['./view-question-dialog.component.scss']
})
export class ViewQuestionDialogComponent implements OnInit {

  form: FormGroup;
  questionId: number;
  editable: boolean;
  questionDto: QuestionDto;
  attachment: string;

  constructor(private dialogRef: MatDialogRef<ViewQuestionDialogComponent>,
              private fb: FormBuilder,
              private questionService: QuestionService,
              @Inject(MAT_DIALOG_DATA) private data: any) {
    this.form = this.fb.group({
      content: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(2000)]],
      points: [1, [Validators.required, Validators.min(0)]],
      answersToDraw: [5, [Validators.required, Validators.min(1)]]
    });
    this.questionId = this.data['questionId'];
    this.editable = this.data['editable'];
    if (!this.editable) {
      this.form.disable();
    }
  }

  ngOnInit() {
    this.questionService.read(this.questionId)
      .subscribe(
        success => {
          this.questionDto = success;
          this.patchQuestionFormValue(success);
        },
        () => {
          this.dialogRef.close();
        }
      );
  }

  patchQuestionFormValue(dto: QuestionDto): void {
    this.form.patchValue({
      content: dto.content,
      points: dto.points,
      answersToDraw: dto.answersToDraw
    });
    this.attachment = dto.attachment;
  }

  getQuestionData(): QuestionDto {
    const result: QuestionDto = this.form.value as QuestionDto;
    result.attachment = this.attachment;
    return result;
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  removeAttachment(): void {
    this.attachment = null;
  }

  downloadAttachment(): void {
    AttachmentService.downloadAttachment(this.attachment);
  }

  onFileChanged(event: Event) {
    const reader = new FileReader();
    if (event.target['files'] && event.target['files'].length === 1) {
      const file = event.target['files'][0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.attachment = AttachmentService.saveAttachment(file.name, reader.result as string);
      };
    }
  }
}

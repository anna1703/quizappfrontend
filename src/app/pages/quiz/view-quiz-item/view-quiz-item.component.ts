import { Component, Input } from '@angular/core';
import { QuizDto } from '../../../core/model/quiz/quiz-dto';
import { Router } from '@angular/router';
import { QuizView } from '../../../core/others/quiz-view.enum';
import { TrialService } from '../../../core/service/trial/trial.service';

@Component({
  selector: 'qa-view-quiz-item',
  templateUrl: './view-quiz-item.component.html',
  styleUrls: ['./view-quiz-item.component.scss']
})
export class ViewQuizItemComponent {

  @Input() view: QuizView;
  @Input() quiz: QuizDto;
  quizViewEnum = QuizView;

  constructor(private router: Router,
              private trialService: TrialService) { }

  viewQuiz(): void {
    this.router.navigate(['/quiz', this.quiz.id]);
  }

  viewQuizInvitations(): void {
    this.router.navigate(['/quiz/', this.quiz.id, 'invitation']);
  }

  solveQuiz(): void {
    this.trialService.solveQuiz(this.quiz.id);
  }

  viewStatistics(): void {
    switch (this.view) {
      case this.quizViewEnum.PUBLIC: {
        this.router.navigate(['/statistics/common', this.quiz.id]);
        break;
      }
      case this.quizViewEnum.SOLVED: {
        this.router.navigate(['/statistics/solver', this.quiz.id]);
        break;
      }
      case this.quizViewEnum.CREATOR: {
        this.router.navigate(['/statistics/creator', this.quiz.id]);
        break;
      }
    }
  }
}

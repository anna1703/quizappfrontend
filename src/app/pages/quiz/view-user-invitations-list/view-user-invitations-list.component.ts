import { Component, OnInit, ViewChild } from '@angular/core';
import { InvitationListDto } from '../../../core/model/quiz/invitation-list-dto';
import { MatPaginator } from '@angular/material';
import { InvitationService } from '../../../core/service/quiz/invitation.service';

@Component({
  selector: 'qa-view-user-invitations-list',
  templateUrl: './view-user-invitations-list.component.html',
  styleUrls: ['./view-user-invitations-list.component.scss']
})
export class ViewUserInvitationsListComponent implements OnInit {

  invitationListDto: InvitationListDto = {
    invitations: [],
    totalCount: 0
  };

  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageSize = 10;
  pageIndex = 0;

  constructor(private invitationService: InvitationService) { }

  ngOnInit() {
    this.readList();
  }

  readList(): void {
    this.invitationService.readForUser(this.pageIndex, this.pageSize)
      .subscribe(res => this.invitationListDto = res);
  }

  changePage(e: any): void {
    this.pageSize = e.pageSize;
    this.pageIndex = e.pageIndex;
    this.readList();
  }
}

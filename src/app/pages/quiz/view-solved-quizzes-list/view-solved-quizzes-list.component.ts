import { Component, OnInit, ViewChild } from '@angular/core';
import { QuizView } from '../../../core/others/quiz-view.enum';
import { FormBuilder, FormGroup } from '@angular/forms';
import { QuizParams } from '../../../core/model/quiz/quiz-params';
import { QuizListDto } from '../../../core/model/quiz/quiz-list-dto';
import { MatPaginator } from '@angular/material';
import { QuizService } from '../../../core/service/quiz/quiz.service';

@Component({
  selector: 'qa-view-solved-quizzes-list',
  templateUrl: './view-solved-quizzes-list.component.html',
  styleUrls: ['./view-solved-quizzes-list.component.scss']
})
export class ViewSolvedQuizzesListComponent implements OnInit {

  quizView = QuizView;
  form: FormGroup;
  filterData: QuizParams = { } as QuizParams;

  quizListDto: QuizListDto = {
    quizzes: [],
    totalCount: 0
  };

  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageSize = 10;
  pageIndex = 0;

  constructor(private fb: FormBuilder,
              private quizService: QuizService) {
    this.form = this.fb.group({
      name: [],
      description: [],
      creatorFirstname: [],
      creatorLastname: []
    });
  }

  ngOnInit() {
    this.readList();
  }

  readList(): void {
    this.quizService.readSolvedList(this.pageIndex, this.pageSize, this.filterData)
      .subscribe(res => this.quizListDto = res);
  }

  changePage(e: any): void {
    this.pageSize = e.pageSize;
    this.pageIndex = e.pageIndex;
    this.readList();
  }

  filter(): void {
    this.filterData = this.form.value as QuizParams;
    this.pageIndex = 0;
    this.readList();
  }
}

import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { AnswerDto } from '../../../core/model/quiz/answer-dto';
import { AttachmentService } from '../../../core/service/common/attachment.service';

@Component({
  selector: 'qa-create-answer-dialog',
  templateUrl: './create-answer-dialog.component.html',
  styleUrls: ['./create-answer-dialog.component.scss']
})
export class CreateAnswerDialogComponent {

  form: FormGroup;
  attachment: string;

  constructor(private dialogRef: MatDialogRef<CreateAnswerDialogComponent>,
              private fb: FormBuilder) {
    this.form = this.fb.group({
      content: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(1000)]],
      isCorrect: [false]
    });
  }

  getAnswerData(): AnswerDto {
    const result: AnswerDto = this.form.value as AnswerDto;
    result.attachment = this.attachment;
    return result;
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  removeAttachment(): void {
    this.attachment = null;
  }

  downloadAttachment(): void {
    AttachmentService.downloadAttachment(this.attachment);
  }

  onFileChanged(event: Event) {
    const reader = new FileReader();
    if (event.target['files'] && event.target['files'].length === 1) {
      const file = event.target['files'][0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.attachment = AttachmentService.saveAttachment(file.name, reader.result as string);
      };
    }
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuizRoutingModule } from './quiz-routing.module';
import { CreateQuizComponent } from './create-quiz/create-quiz.component';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateQuestionDialogComponent } from './create-question-dialog/create-question-dialog.component';
import { CreateAnswerDialogComponent } from './create-answer-dialog/create-answer-dialog.component';
import { ViewCreatedQuizzesListComponent } from './view-created-quizzes-list/view-created-quizzes-list.component';
import { ViewQuizFullComponent } from './view-quiz-full/view-quiz-full.component';
import { ViewAnswerDialogComponent } from './view-answer-dialog/view-answer-dialog.component';
import { ViewQuestionDialogComponent } from './view-question-dialog/view-question-dialog.component';
import { ViewQuizItemComponent } from './view-quiz-item/view-quiz-item.component';
import { CreateInvitationDialogComponent } from './create-invitation-dialog/create-invitation-dialog.component';
import { ViewQuizInvitationsListComponent } from './view-quiz-invitations-list/view-quiz-invitations-list.component';
import { ViewInvitationItemComponent } from './view-invitation-item/view-invitation-item.component';
import { ViewUserInvitationsListComponent } from './view-user-invitations-list/view-user-invitations-list.component';
import { ViewSolvedQuizzesListComponent } from './view-solved-quizzes-list/view-solved-quizzes-list.component';
import { ViewPublicQuizzesListComponent } from './view-public-quizzes-list/view-public-quizzes-list.component';

@NgModule({
  imports: [
    CommonModule,
    QuizRoutingModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    CreateQuizComponent,
    CreateQuestionDialogComponent,
    CreateAnswerDialogComponent,
    ViewCreatedQuizzesListComponent,
    ViewQuizFullComponent,
    ViewAnswerDialogComponent,
    ViewQuestionDialogComponent,
    ViewQuizItemComponent,
    CreateInvitationDialogComponent,
    ViewQuizInvitationsListComponent,
    ViewInvitationItemComponent,
    ViewUserInvitationsListComponent,
    ViewSolvedQuizzesListComponent,
    ViewPublicQuizzesListComponent
  ],
  entryComponents: [
    CreateInvitationDialogComponent,
    CreateQuestionDialogComponent,
    CreateAnswerDialogComponent,
    ViewAnswerDialogComponent,
    ViewQuestionDialogComponent
  ]
})
export class QuizModule { }

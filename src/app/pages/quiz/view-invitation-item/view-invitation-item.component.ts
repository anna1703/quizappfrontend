import { Component, Input, OnInit } from '@angular/core';
import { InvitationDto } from '../../../core/model/quiz/invitation-dto';
import * as moment from 'moment';
import { TrialService } from '../../../core/service/trial/trial.service';
import { InvitationService } from '../../../core/service/quiz/invitation.service';

@Component({
  selector: 'qa-view-invitation-item',
  templateUrl: './view-invitation-item.component.html',
  styleUrls: ['./view-invitation-item.component.scss']
})
export class ViewInvitationItemComponent implements OnInit {

  @Input() isCreatorView: boolean;
  @Input() invitation: InvitationDto;

  constructor(private trialService: TrialService,
              private invitationService: InvitationService) { }

  ngOnInit() {
    if (this.invitation.dateFrom) {
      this.invitation.dateFrom = moment(this.invitation.dateFrom).locale('pl');
    }
    if (this.invitation.dateTo) {
      this.invitation.dateTo = moment(this.invitation.dateTo).locale('pl');
    }
  }

  solveQuiz(): void {
    this.trialService.solveQuiz(this.invitation.quiz.id);
  }

  isInvitationValid(): boolean {
    return this.invitationService.isInvitationValid(this.invitation);
  }
}

import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { QuestionDto } from '../../../core/model/quiz/question-dto';
import { AttachmentService } from '../../../core/service/common/attachment.service';

@Component({
  selector: 'qa-create-question-dialog',
  templateUrl: './create-question-dialog.component.html',
  styleUrls: ['./create-question-dialog.component.scss']
})
export class CreateQuestionDialogComponent {

  form: FormGroup;
  attachment: string;

  constructor(private dialogRef: MatDialogRef<CreateQuestionDialogComponent>,
              private fb: FormBuilder) {
    this.form = this.fb.group({
      content: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(2000)]],
      points: [1, [Validators.required, Validators.min(0)]],
      answersToDraw: [5, [Validators.required, Validators.min(1)]]
    });
  }

  getQuestionData(): QuestionDto {
    const result: QuestionDto = this.form.value as QuestionDto;
    result.attachment = this.attachment;
    return result;
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  removeAttachment(): void {
    this.attachment = null;
  }

  downloadAttachment(): void {
    AttachmentService.downloadAttachment(this.attachment);
  }

  onFileChanged(event: Event) {
    const reader = new FileReader();
    if (event.target['files'] && event.target['files'].length === 1) {
      const file = event.target['files'][0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.attachment = AttachmentService.saveAttachment(file.name, reader.result as string);
      };
    }
  }
}

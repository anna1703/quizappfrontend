import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { QuizService } from '../../../core/service/quiz/quiz.service';
import { QuizDto } from '../../../core/model/quiz/quiz-dto';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { CreateQuestionDialogComponent } from '../create-question-dialog/create-question-dialog.component';
import { QuestionDto } from '../../../core/model/quiz/question-dto';
import { QuestionService } from '../../../core/service/quiz/question.service';
import { MessageService } from '../../../core/service/common/message.service';
import { QuizFullDto } from '../../../core/model/quiz/quiz-full-dto';
import { QuestionFullDto } from '../../../core/model/quiz/question-full-dto';
import { CreateAnswerDialogComponent } from '../create-answer-dialog/create-answer-dialog.component';
import { AnswerDto } from '../../../core/model/quiz/answer-dto';
import { AnswerService } from '../../../core/service/quiz/answer.service';
import { ViewQuestionDialogComponent } from '../view-question-dialog/view-question-dialog.component';
import { ViewAnswerDialogComponent } from '../view-answer-dialog/view-answer-dialog.component';

@Component({
  selector: 'qa-view-quiz-full',
  templateUrl: './view-quiz-full.component.html',
  styleUrls: ['./view-quiz-full.component.scss']
})
export class ViewQuizFullComponent implements OnInit {

  quizForm: FormGroup;
  quizFullDto: QuizFullDto = {
    quiz: { } as QuizDto,
    questions: []
  };

  constructor(private fb: FormBuilder,
              private quizService: QuizService,
              private questionService: QuestionService,
              private answerService: AnswerService,
              private messageService: MessageService,
              private router: Router,
              private route: ActivatedRoute,
              private dialog: MatDialog) {
    this.quizForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(300)]],
      description: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(1000)]],
      isPrivate: [false],
      possibleTrials: [1, [Validators.required, Validators.min(1)]],
      time: [30, [Validators.required, Validators.min(1)]],
      questionsToDraw: [5, [Validators.required, Validators.min(1)]]
    });
  }

  ngOnInit() {
    this.quizService.readWithDetails(+this.route.snapshot.paramMap.get('id'))
      .subscribe(res => {
        this.quizFullDto = res;
        this.patchQuizFormValue(res.quiz);
      });
  }

  patchQuizFormValue(dto: QuizDto): void {
    this.quizForm.patchValue({
      name: dto.name,
      description: dto.description,
      isPrivate: dto.isPrivate,
      possibleTrials: dto.possibleTrials,
      time: dto.time,
      questionsToDraw: dto.questionsToDraw
    });
    if (!this.isEditableAndRemovable()) {
      this.quizForm.disable();
    }
  }

  getQuizData(): QuizDto {
    return this.quizForm.value as QuizDto;
  }

  updateQuiz(): void {
    this.quizService.update(this.quizFullDto.quiz.id, this.getQuizData())
      .subscribe(
        success => {
          this.messageService.displaySuccessMessage('Quiz został zaktualizowany');
          this.quizFullDto.quiz = success;
          this.patchQuizFormValue(success);
        },
        error => {
          this.messageService.displayErrorMessage(error);
        }
      );
  }

  deleteQuiz(): void {
    this.quizService.delete(this.quizFullDto.quiz.id)
      .subscribe(
        () => {
          this.messageService.displaySuccessMessage('Quiz został usunięty');
          this.router.navigate(['/quiz/created']);
        },
        error => {
          this.messageService.displayErrorMessage(error);
        }
      );
  }

  isEditableAndRemovable(): boolean {
    return this.quizFullDto.quiz.inPreparation;
  }

  openNewQuestionDialog(): void {
    const dialogRef = this.dialog.open(CreateQuestionDialogComponent, {
      width: '500px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.createQuestion(result);
      }
    });
  }

  createQuestion(questionDto: QuestionDto): void {
    this.questionService.create(questionDto, this.quizFullDto.quiz.id)
      .subscribe(
        dto => {
          this.messageService.displaySuccessMessage('Pytanie zostało utworzone');
          this.quizFullDto.questions.push({
            question: dto,
            answers: []
          } as QuestionFullDto);
        },
        error => {
          this.messageService.displayErrorMessage(error);
        }
      );
  }

  openNewAnswerDialog(questionId: number, questionIndex: number): void {
    const dialogRef = this.dialog.open(CreateAnswerDialogComponent, {
      width: '500px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.createAnswer(result, questionId, questionIndex);
      }
    });
  }

  createAnswer(answerDto: AnswerDto, questionId: number, questionIndex: number): void {
    this.answerService.create(answerDto, questionId)
      .subscribe(
        dto => {
          this.messageService.displaySuccessMessage('Odpowiedź została utworzona');
          this.quizFullDto.questions[questionIndex].answers.push(dto);
        },
        error => {
          this.messageService.displayErrorMessage(error);
        }
      );
  }

  deleteQuestion(questionId: number, questionIndex: number): void {
    this.questionService.delete(questionId)
      .subscribe(
        () => {
          this.messageService.displaySuccessMessage('Pytanie zostało usunięte');
          this.quizFullDto.questions.splice(questionIndex, 1);
        },
        error => {
          this.messageService.displayErrorMessage(error);
        }
      );
  }

  deleteAnswer(answerId: number, questionIndex: number, answerIndex: number): void {
    this.answerService.delete(answerId)
      .subscribe(
        () => {
          this.messageService.displaySuccessMessage('Odpowiedź została usunięta');
          this.quizFullDto.questions[questionIndex].answers.splice(answerIndex, 1);
        },
        error => {
          this.messageService.displayErrorMessage(error);
        }
      );
  }

  openQuestionDialog(questionId: number, questionIndex: number): void {
    const dialogRef = this.dialog.open(ViewQuestionDialogComponent, {
      width: '500px',
      data: {
        questionId: questionId,
        editable: this.isEditableAndRemovable()
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.updateQuestion(result, questionId, questionIndex);
      }
    });
  }

  updateQuestion(questionDto: QuestionDto, questionId: number, questionIndex: number): void {
    this.questionService.update(questionId, questionDto)
      .subscribe(
        success => {
          this.messageService.displaySuccessMessage('Pytanie zostało zaktualizowane');
          this.quizFullDto.questions[questionIndex].question = success;
        },
        error => {
          this.messageService.displayErrorMessage(error);
        }
      );
  }

  openAnswerDialog(answerId: number, questionIndex: number, answerIndex: number): void {
    const dialogRef = this.dialog.open(ViewAnswerDialogComponent, {
      width: '500px',
      data: {
        answerId: answerId,
        editable: this.isEditableAndRemovable()
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.updateAnswer(result, answerId, questionIndex, answerIndex);
      }
    });
  }

  updateAnswer(answerDto: AnswerDto, answerId: number, questionIndex: number, answerIndex: number): void {
    this.answerService.update(answerId, answerDto)
      .subscribe(
        success => {
          this.messageService.displaySuccessMessage('Odpowiedź została zaktualizowana');
          this.quizFullDto.questions[questionIndex].answers[answerIndex] = success;
        },
        error => {
          this.messageService.displayErrorMessage(error);
        }
      );
  }

  publish(): void {
    this.quizService.publish(this.quizFullDto.quiz.id)
      .subscribe(
        success => {
          this.messageService.displaySuccessMessage('Quiz został opublikowany');
          this.quizFullDto.quiz = success;
          this.patchQuizFormValue(success);
        },
        error => {
          this.messageService.displayErrorMessage(error);
        }
      );
  }
}

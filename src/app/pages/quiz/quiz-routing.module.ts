import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateQuizComponent } from './create-quiz/create-quiz.component';
import { ViewCreatedQuizzesListComponent } from './view-created-quizzes-list/view-created-quizzes-list.component';
import { ViewQuizFullComponent } from './view-quiz-full/view-quiz-full.component';
import { ViewQuizInvitationsListComponent } from './view-quiz-invitations-list/view-quiz-invitations-list.component';
import { ViewUserInvitationsListComponent } from './view-user-invitations-list/view-user-invitations-list.component';
import { ViewSolvedQuizzesListComponent } from './view-solved-quizzes-list/view-solved-quizzes-list.component';
import { ViewPublicQuizzesListComponent } from './view-public-quizzes-list/view-public-quizzes-list.component';

const routes: Routes = [{
  path: 'created',
  component: ViewCreatedQuizzesListComponent
}, {
  path: 'solved',
  component: ViewSolvedQuizzesListComponent
}, {
  path: 'public',
  component: ViewPublicQuizzesListComponent
}, {
  path: 'new',
  component: CreateQuizComponent
}, {
  path: 'invitation',
  component: ViewUserInvitationsListComponent
}, {
  path: ':id',
  component: ViewQuizFullComponent
}, {
  path: ':id/invitation',
  component: ViewQuizInvitationsListComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuizRoutingModule { }

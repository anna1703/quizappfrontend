import { Component } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { QuizDto } from '../../../core/model/quiz/quiz-dto';
import { QuizService } from '../../../core/service/quiz/quiz.service';
import { Router } from '@angular/router';
import { MessageService } from '../../../core/service/common/message.service';

@Component({
  selector: 'qa-create-quiz',
  templateUrl: './create-quiz.component.html',
  styleUrls: ['./create-quiz.component.scss']
})
export class CreateQuizComponent {

  form: FormGroup;

  constructor(private fb: FormBuilder,
              private quizService: QuizService,
              private router: Router,
              private messageService: MessageService) {
    this.form = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(300)]],
      description: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(1000)]],
      isPrivate: [false],
      possibleTrials: [1, [Validators.required, Validators.min(1)]],
      time: [30, [Validators.required, Validators.min(1)]],
      questionsToDraw: [5, [Validators.required, Validators.min(1)]]
    });
  }

  getQuizData(): QuizDto {
    return this.form.value as QuizDto;
  }

  createQuiz(): void {
    this.quizService.create(this.getQuizData())
      .subscribe(
        dto => {
          this.messageService.displaySuccessMessage('Quiz został utworzony');
          this.router.navigate(['/quiz', dto.id]);
        },
        error => {
          this.messageService.displayErrorMessage(error);
        }
      );
  }

  goBackToList(): void {
    this.router.navigate(['/quiz/created']);
  }
}

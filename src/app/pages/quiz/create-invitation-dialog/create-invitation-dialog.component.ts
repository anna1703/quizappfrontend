import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InvitationDto } from '../../../core/model/quiz/invitation-dto';
import { UserService } from '../../../core/service/user/user.service';
import { UserListDto } from '../../../core/model/user/user-list-dto';
import { UserDto } from '../../../core/model/user/user-dto';
import { map, debounceTime } from 'rxjs/operators';
import { propertyRequiredValidator } from '../../../core/validators/property-required.validator';
import { datesValidator } from '../../../core/validators/dates.validator';

@Component({
  selector: 'qa-create-invitation-dialog',
  templateUrl: './create-invitation-dialog.component.html',
  styleUrls: ['./create-invitation-dialog.component.scss']
})
export class CreateInvitationDialogComponent implements OnInit {

  form: FormGroup;
  filteredUsers: UserListDto = {
    users: []
  };

  constructor(private dialogRef: MatDialogRef<CreateInvitationDialogComponent>,
              private fb: FormBuilder,
              private userService: UserService) {
    this.form = this.fb.group({
      user: [{}, [Validators.required, propertyRequiredValidator('id')]],
      dateFrom: [],
      dateTo: []
    }, { validators: datesValidator('dateFrom', 'dateTo')});
  }

  ngOnInit() {
    this.form.get('user').valueChanges
      .pipe(
        debounceTime(600),
        map(value => typeof value === 'string' ? value : value.email)
      )
      .subscribe(value => this.findUsers(value));
  }

  displayFn(user?: UserDto): string | undefined {
    return user ? user.email : undefined;
  }

  findUsers(email: string): void {
    this.userService.readListByEmail(email)
      .subscribe(result => this.filteredUsers = result);
  }

  getInvitationData(): InvitationDto {
    const result: InvitationDto = this.form.value as InvitationDto;
    if (result.dateTo) {
      result.dateTo.endOf('day');
    }
    return this.form.value as InvitationDto;
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }
}
